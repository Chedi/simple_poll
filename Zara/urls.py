from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

admin.autodiscover()

urlpatterns = patterns('',
	url(r'^$'            , 'poll.views.home'      , name='home'      ),
	url(r'^add_survey/$' , 'poll.views.add_survey', name='add_survey'),
	url(r'^error/$'      , 'poll.views.error'     , name="error"     ),

	url(r'^admin/css/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT+"/admin/css"}),
	url(r'^admin/js/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT+"/admin/js"}),
	url(r'^admin/img/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT+"/admin/img"}),
	url(r'^admin/', include(admin.site.urls )),
	url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
	url(r'^bootstrap/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT+"/bootstrap"}),
)