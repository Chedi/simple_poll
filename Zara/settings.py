import os

ROOT_PATH      = os.path.dirname(__file__)
DEBUG          = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Chedi Toueiti', 'chedi.toueiti@gmail.com'),
)

MANAGERS     = ADMINS
INTERNAL_IPS = ('127.0.0.1',)

DATABASES = {
    'default': {
        'ENGINE'  : 'django.db.backends.postgresql_psycopg2',
        'NAME'    : 'zara',
        'USER'    : 'postgres',
        'PASSWORD': '',
        'HOST'    : 'localhost',
        'PORT'    : '5432',
    }
}

TIME_ZONE          = 'Africa/Tunis'
LANGUAGE_CODE      = 'en-us'
SITE_ID            = 1
USE_I18N           = True
USE_L10N           = True
MEDIA_URL          = '/media'
STATIC_URL         = '/static'
MEDIA_ROOT         = os.path.abspath(os.path.join(ROOT_PATH, "../media"))
STATIC_ROOT        = os.path.abspath(os.path.join(ROOT_PATH, "../static"))
STATICFILES_DIRS   = ()

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SECRET_KEY = '70o=u1+i^2xj+@l1w3g(exc^3xjqnq=bcggc6_7o#%q@x&f(-q'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'Zara.urls'

TEMPLATE_DIRS = (os.path.join(ROOT_PATH, "../templates"),)

INSTALLED_APPS = (
    'django_admin_bootstrapped',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'annoying',
    'poll',
     #'south',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

WSGI_APPLICATION= 'Zara.wsgi.application'
