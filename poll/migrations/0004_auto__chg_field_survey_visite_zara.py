# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Survey.visite_zara'
        db.alter_column('poll_survey', 'visite_zara', self.gf('django.db.models.fields.IntegerField')(default=1))

    def backwards(self, orm):

        # Changing field 'Survey.visite_zara'
        db.alter_column('poll_survey', 'visite_zara', self.gf('django.db.models.fields.IntegerField')(null=True))

    models = {
        'poll.accompagnement': {
            'Meta': {'object_name': 'Accompagnement'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'poll.survey': {
            'Meta': {'object_name': 'Survey'},
            'accompagne': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'accompagne_type': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['poll.Accompagnement']", 'null': 'True', 'blank': 'True'}),
            'activite': ('django.db.models.fields.IntegerField', [], {}),
            'age': ('django.db.models.fields.IntegerField', [], {}),
            'boutique_gc': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment_arrivez': ('django.db.models.fields.IntegerField', [], {}),
            'date_survey': ('django.db.models.fields.DateField', [], {}),
            'enqueteur': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'fast_food_gc': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lieu_arrivez': ('django.db.models.fields.IntegerField', [], {}),
            'magasin_geant_visite': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'nbre_personnes': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'niveau': ('django.db.models.fields.IntegerField', [], {}),
            'numero_quest': ('django.db.models.fields.IntegerField', [], {}),
            'profession': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'quartier': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'residence': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'sexe': ('django.db.models.fields.IntegerField', [], {}),
            'statut': ('django.db.models.fields.IntegerField', [], {}),
            'superviseur': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'telephone': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'time_arrive': ('django.db.models.fields.TimeField', [], {}),
            'time_survey': ('django.db.models.fields.TimeField', [], {}),
            'visite_magasin_geant': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'visite_zara': ('django.db.models.fields.IntegerField', [], {}),
            'voiture': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['poll']