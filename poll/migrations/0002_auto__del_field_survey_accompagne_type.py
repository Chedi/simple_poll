# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Survey.accompagne_type'
        db.delete_column('poll_survey', 'accompagne_type')

        # Adding M2M table for field accompagne_type on 'Survey'
        db.create_table('poll_survey_accompagne_type', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('survey', models.ForeignKey(orm['poll.survey'], null=False)),
            ('accompagnement', models.ForeignKey(orm['poll.accompagnement'], null=False))
        ))
        db.create_unique('poll_survey_accompagne_type', ['survey_id', 'accompagnement_id'])


    def backwards(self, orm):
        # Adding field 'Survey.accompagne_type'
        db.add_column('poll_survey', 'accompagne_type',
                      self.gf('django.db.models.fields.IntegerField')(null=True),
                      keep_default=False)

        # Removing M2M table for field accompagne_type on 'Survey'
        db.delete_table('poll_survey_accompagne_type')


    models = {
        'poll.accompagnement': {
            'Meta': {'object_name': 'Accompagnement'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'poll.survey': {
            'Meta': {'object_name': 'Survey'},
            'accompagne': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'accompagne_type': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['poll.Accompagnement']", 'null': 'True', 'symmetrical': 'False'}),
            'activite': ('django.db.models.fields.IntegerField', [], {}),
            'age': ('django.db.models.fields.IntegerField', [], {}),
            'boutique_gc': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment_arrivez': ('django.db.models.fields.IntegerField', [], {}),
            'date_survey': ('django.db.models.fields.DateField', [], {}),
            'enqueteur': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'fast_food_gc': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lieu_arrivez': ('django.db.models.fields.IntegerField', [], {}),
            'magasin_geant_visite': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'nbre_personnes': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'niveau': ('django.db.models.fields.IntegerField', [], {}),
            'numero_quest': ('django.db.models.fields.IntegerField', [], {}),
            'profession': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'quartier': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'residence': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'sexe': ('django.db.models.fields.IntegerField', [], {}),
            'statut': ('django.db.models.fields.IntegerField', [], {}),
            'superviseur': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'telephone': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'time_arrive': ('django.db.models.fields.TimeField', [], {}),
            'time_survey': ('django.db.models.fields.TimeField', [], {}),
            'visite_magasin_geant': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'visite_zara': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'voiture': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['poll']