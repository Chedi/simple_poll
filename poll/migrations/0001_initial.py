# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Accompagnement'
        db.create_table('poll_accompagnement', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('poll', ['Accompagnement'])

        # Adding model 'Survey'
        db.create_table('poll_survey', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('time_survey', self.gf('django.db.models.fields.TimeField')()),
            ('time_arrive', self.gf('django.db.models.fields.TimeField')()),
            ('date_survey', self.gf('django.db.models.fields.DateField')()),
            ('accompagne', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('boutique_gc', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('fast_food_gc', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('numero_quest', self.gf('django.db.models.fields.IntegerField')()),
            ('magasin_geant_visite', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('voiture', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('quartier', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('residence', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('telephone', self.gf('django.db.models.fields.CharField')(max_length=8)),
            ('enqueteur', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('profession', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('superviseur', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('nbre_personnes', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('accompagne_type', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('age', self.gf('django.db.models.fields.IntegerField')()),
            ('sexe', self.gf('django.db.models.fields.IntegerField')()),
            ('niveau', self.gf('django.db.models.fields.IntegerField')()),
            ('statut', self.gf('django.db.models.fields.IntegerField')()),
            ('activite', self.gf('django.db.models.fields.IntegerField')()),
            ('lieu_arrivez', self.gf('django.db.models.fields.IntegerField')()),
            ('comment_arrivez', self.gf('django.db.models.fields.IntegerField')()),
            ('visite_zara', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('visite_magasin_geant', self.gf('django.db.models.fields.IntegerField')(null=True)),
        ))
        db.send_create_signal('poll', ['Survey'])


    def backwards(self, orm):
        # Deleting model 'Accompagnement'
        db.delete_table('poll_accompagnement')

        # Deleting model 'Survey'
        db.delete_table('poll_survey')


    models = {
        'poll.accompagnement': {
            'Meta': {'object_name': 'Accompagnement'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'poll.survey': {
            'Meta': {'object_name': 'Survey'},
            'accompagne': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'accompagne_type': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'activite': ('django.db.models.fields.IntegerField', [], {}),
            'age': ('django.db.models.fields.IntegerField', [], {}),
            'boutique_gc': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment_arrivez': ('django.db.models.fields.IntegerField', [], {}),
            'date_survey': ('django.db.models.fields.DateField', [], {}),
            'enqueteur': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'fast_food_gc': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lieu_arrivez': ('django.db.models.fields.IntegerField', [], {}),
            'magasin_geant_visite': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'nbre_personnes': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'niveau': ('django.db.models.fields.IntegerField', [], {}),
            'numero_quest': ('django.db.models.fields.IntegerField', [], {}),
            'profession': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'quartier': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'residence': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'sexe': ('django.db.models.fields.IntegerField', [], {}),
            'statut': ('django.db.models.fields.IntegerField', [], {}),
            'superviseur': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'telephone': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'time_arrive': ('django.db.models.fields.TimeField', [], {}),
            'time_survey': ('django.db.models.fields.TimeField', [], {}),
            'visite_magasin_geant': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'visite_zara': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'voiture': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['poll']