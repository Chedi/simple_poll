# coding=utf8
# -*- coding: utf8 -*-

accompagne_type_choices = (
	(1, "Amis"                ),
	(2, "Enfant"              ),
	(3, "Epoux/Epouse"        ),
	(4, "Parents"             ),
	(5, "Membre de la famille"),
	(6, "Seul"),
)

activite_choices = (
	(1, "Au Foyer"),
	(2, "Actif"   ),
	(3, "Etudiant"),
	(4, "Autre"   ),
)

age_choices = (
	(1, "15-24"     ),
	(2, "25-34"     ),
	(3, "35-49"     ),
	(4, "50 et plus"),
)

comment_arrivez_choices = (
	(1, "Voiture perso"),
	(2, "Bus ou Métro" ),
	(3, "Vélo - Moto"  ),
	(4, "Taxi"         ),
	(5, "Accompagné"   ),
	(6, "A pied"       ),
)

lieu_arrivez_choices = (
	(1, "Domicile"             ),
	(2, "Loisirs"              ),
	(3, "Commerce de proximité"),
	(4, "Bureau - activité"    ),
	(5, "Visite ami parent"    ),
	(6, "Autre"    ),
)

niveau_choices = (
	(1, "Primaire"     ),
	(2, "Secondaire"   ),
	(3, "Universitaire"),
)

sexe_choices = (
	(1, "Homme"),
	(2, "Femme"),
)

statut_choices = (
	(1, "Marié"      ),
	(2, "Célibataire"),
	(3, "Autre"      ),
)

visite_magasin_choices = (
	(1, "1 fois / semaine"                             ),
	(2, "1 fois / mois"                                ),
	(3, "Plus d'une fois / semaine"                    ),
	(4, "Plus d'une fois / mois"                       ),
	(5, "Que pour un achat particulier - solde - promo"),
)


quartier_choices = (
	("1", "1 - Tunis centre ville"                     ),
	("2", "2 - Marsa, carthage, Sidi Bousaid, Gammarth"),
	("3", "3 - Kram, La goulette"                      ),
	("4", "4 - Mutuelle ville, Menzah 1, Cité jardin"  ),
	("5", "5 - El oumrane, Bardo"                      ),
	("6", "6 - Manar, Menzah 5..9"                     ),
	("7", "7 - Ariana, Melaha, Ennozha"                ),
	("8", "8 - Enasr, Riadh el andalous"               ),
	("9", "9 - El ghazala, Borj el ouzir"              ),
	("10", "10 - Soukra, ain zaghouen"                 ),
	("11", "11 - Lac 1/2"                              ),
	("12", "12 - Manouba, oued el lil, Ksar said"      ),
	("13", "13 - Jardin el menzh"                      ),
	("14", "14 - Intilaka, Ettadhamon, Mnihla"         ),
	("15", "15 - Lakanya, Wardia, Jbel el jloud"       ),
	("16", "16 - Megrine, Rades, Ezzahra"              ),
	("17", "17 - Hammam lif, Borj essedria, Boumhal"   ),
	("18", "18 - Montfleury, Rabta, Bab saïdoun"       ),
	("19", "19 - Sijoumi, Ezzouhour, El Akba, Dandan"  ),
	("20", "20 - Ben Arous, Madina el jadida"          ),
	("21", "21 - Mourouj, Ibn sina"                    ),
	("22", "22 - Sousse"                               ),
	("23", "23 - Sfax"                                 ),
	("24", "24 - Bizerte"                              ),
	("25", "25 - Nabeul"                               ),
	("26", "26 - Beja"                                 ),
	("27", "27 - zaghouen"                             ),
	("28", "28 - Autre"                                ),
	)