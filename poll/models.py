from choices   import *
from django.db import models

class Accompagnement(models.Model):
	label = models.CharField(max_length=50)
	def __unicode__(self):
		return self.label

class Survey(models.Model):
	time_survey          = models.TimeField()
	time_arrive          = models.TimeField()
	date_survey          = models.DateField()

	accompagne           = models.BooleanField()
	boutique_gc          = models.BooleanField()
	fast_food_gc         = models.BooleanField()
	numero_quest         = models.IntegerField()
	magasin_geant_visite = models.BooleanField()

	voiture              = models.CharField(max_length=100)
	quartier             = models.CharField(max_length=100, choices=quartier_choices)
	residence            = models.CharField(max_length=100, choices=quartier_choices)
	profession           = models.CharField(max_length=100)
	telephone            = models.CharField(max_length=8  )
	enqueteur            = models.CharField(null=True, blank=True, max_length=100)
	superviseur          = models.CharField(null=True, blank=True, max_length=100)

	nbre_personnes       = models.IntegerField(null=True, blank=True)
	accompagne_type      = models.ManyToManyField(Accompagnement, null=True, blank=True)

	age                  = models.IntegerField(choices=age_choices            )
	sexe                 = models.IntegerField(choices=sexe_choices           )
	niveau               = models.IntegerField(choices=niveau_choices         )
	statut               = models.IntegerField(choices=statut_choices         )
	activite             = models.IntegerField(choices=activite_choices       )
	lieu_arrivez         = models.IntegerField(choices=lieu_arrivez_choices   )
	comment_arrivez      = models.IntegerField(choices=comment_arrivez_choices)

	visite_zara          = models.IntegerField(choices=visite_magasin_choices )
	visite_magasin_geant = models.IntegerField(null=True, blank=True, choices=visite_magasin_choices )

	def __unicode__(self):
		return "%s (%s  %s)" % (self.numero_quest, self.date_survey, self.time_survey)