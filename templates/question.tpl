<div class="row-fluid">
	<div class="span1 offset1">
		<h3>
			<strong>
				{% block question_number %}{% endblock %}
			</strong>
		</h3>
	</div>
	<div class="span10">
		<h3>
			<strong>
				{% block question_label %}{% endblock %}
			</strong>
		</h3>
	</div>
</div>