<div class="row-fluid">
	<div class="span6">
		<div class="row-fluid control-group">
			<label class="span3">
				<strong>Sexe</strong>
			</label>
			<div class="controls span3">
				<select name="sexe" id="sexe" required="required">
					<option value="0">--------</option>
					<option value="1">Homme</option>
					<option value="2">Femme</option>
				</select>
			</div>
		</div>
		<div class="row-fluid control-group">
			<label class="span3" for="age">
				<strong>Age</strong>
			</label>
			<div class="controls span3">
				<select name="age" id="age" required="required">
					<option value="0">--------</option>
					<option value="1">15-24</option>
					<option value="2">25-34</option>
					<option value="3">35-49</option>
					<option value="4">50 et plus</option>
				</select>
			</div>
		</div>
		<div class="row-fluid control-group">
			<label class="span3" for="statut">
				<strong>Statut</strong>
			</label>
			<div class="controls span3">
				<select name="statut" id="statut" required="required">
					<option value="0">--------</option>
					<option value="1">Marié</option>
					<option value="2">Célibataire</option>
					<option value="3">Autre</option>
				</select>
			</div>
		</div>
		<div class="row-fluid control-group">
			<label class="span3" for="niveau">
				<strong>Niveau</strong>
			</label>
			<div class="controls span3">
				<select name="niveau" id="niveau" required="required">
					<option value="0">--------</option>
					<option value="1">Primaire</option>
					<option value="2">Secondaire</option>
					<option value="3">Universitaire</option>
				</select>
			</div>
		</div>
		<div class="row-fluid control-group">
			<label class="span3" for="activite">
				<strong>Activité</strong>
			</label>
			<div class="controls span3">
				<select name="activite" id="activite" required="required">
					<option value="0">--------</option>
					<option value="1">Au Foyer</option>
					<option value="2">Actif</option>
					<option value="3">Etudiant</option>
					<option value="4">Autre</option>
				</select>
			</div>
		</div>
	</div>
	<div class="span6">
		<div class="row-fluid control-group">
			<label class="span3" for="profession">
				<strong>Profession</strong>
			</label>
			<div class="controls span3">
				<input type="text" name="profession" id="profession" required="required" pattern="[a-zA-Z]*"/>
			</div>
		</div>
		<div class="row-fluid control-group">
			<label class="span3" for="voiture">
				<strong>Marque de voiture</strong>
			</label>
			<div class="controls span3">
				<input type="text" name="voiture" id="voiture" required="required"/>
			</div>
		</div>
		<div class="row-fluid control-group">
			<label class="span3" for="residence">
				<strong>Quatier de résidence</strong>
			</label>
			<div class="controls span3">
				<select name="residence" id="residence" required="required">
					<option value="1">"1 - Tunis centre ville"</option>
					<option value="2">"2 - Marsa, carthage, Sidi Bousaid, Gammarth"</option>
					<option value="3">"3 - Kram, La goulette"</option>
					<option value="4">"4 - Mutuelle ville, Menzah 1, Cité jardin"</option>
					<option value="5">"5 - El oumrane, Bardo"</option>
					<option value="6">"6 - Manar, Menzah 5..9"</option>
					<option value="7">"7 - Ariana, Melaha, Ennozha"</option>
					<option value="8">"8 - Enasr, Riadh el andalous"</option>
					<option value="9">"9 - El ghazala, Borj el ouzir"</option>
					<option value="10">"10 - Soukra, ain zaghouen"</option>
					<option value="11">"11 - Lac 1/2"</option>
					<option value="12">"12 - Manouba, oued el lil, Ksar said"</option>
					<option value="13">"13 - Jardin el menzh"</option>
					<option value="14">"14 - Intilaka, Ettadhamon, Mnihla"</option>
					<option value="15">"15 - Lakanya, Wardia, Jbel el jloud"</option>
					<option value="16">"16 - Megrine, Rades, Ezzahra"</option>
					<option value="17">"17 - Hammam lif, Borj essedria, Boumhal"</option>
					<option value="18">"18 - Montfleury, Rabta, Bab saïdoun"</option>
					<option value="19">"19 - Sijoumi, Ezzouhour, El Akba, Dandan"</option>
					<option value="20">"20 - Ben Arous, Madina el jadida"</option>
					<option value="21">"21 - Mourouj, Ibn sina"</option>
					<option value="22">"22 - Sousse, Sfax, Bizerte, Nabeul, Beja"</option>
					<option value="23">"23 - Autre</option>
				</select>
			</div>
		</div>
		<div class="row-fluid control-group">
			<label class="span3" for="telephone">
				<strong>Téléphone</strong>
			</label>
			<div class="controls span3">
				<input type="text" name="telephone" id="telephone" required="required" pattern="[0-9]{8}"/>
			</div>
		</div>
	</div>
</div>