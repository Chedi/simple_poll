<div class="row-fluid control-group">
	<div class="span1">
		<strong>Q4</strong>
	</div>
	<label class="span3" for="lieu_arrivez">
		<strong>De quel lieu êtes vous arrivez ?</strong>
	</label>
	<div class="controls span8">
		<select name="lieu_arrivez" id="lieu_arrivez" required="required">
			<option value="0">--------</option>
			<option value="1">Domicile</option>
			<option value="2">Loisirs</option>
			<option value="3">Commerce de proximité</option>
			<option value="4">Bureau - activité</option>
			<option value="5">Visite ami parent</option>
			<option value="6">Autre</option>
		</select>
	</div>
</div>
<div class="row-fluid control-group">
	<label class="span3 offset1" for="quartier">
		<strong>Quartier</strong>
	</label>
	<div class="controls span8">
		<select name="quartier" id="quartier" required="required">
			<option value="1">"1 - Tunis centre ville"</option>
			<option value="2">"2 - Marsa, carthage, Sidi Bousaid, Gammarth"</option>
			<option value="3">"3 - Kram, La goulette"</option>
			<option value="4">"4 - Mutuelle ville, Menzah 1, Cité jardin"</option>
			<option value="5">"5 - El oumrane, Bardo"</option>
			<option value="6">"6 - Manar, Menzah 5..9"</option>
			<option value="7">"7 - Ariana, Melaha, Ennozha"</option>
			<option value="8">"8 - Enasr, Riadh el andalous"</option>
			<option value="9">"9 - El ghazala, Borj el ouzir"</option>
			<option value="10">"10 - Soukra, ain zaghouen"</option>
			<option value="11">"11 - Lac 1/2"</option>
			<option value="12">"12 - Manouba, oued el lil, Ksar said"</option>
			<option value="13">"13 - Jardin el menzh"</option>
			<option value="14">"14 - Intilaka, Ettadhamon, Mnihla"</option>
			<option value="15">"15 - Lakanya, Wardia, Jbel el jloud"</option>
			<option value="16">"16 - Megrine, Rades, Ezzahra"</option>
			<option value="17">"17 - Hammam lif, Borj essedria, Boumhal"</option>
			<option value="18">"18 - Montfleury, Rabta, Bab saïdoun"</option>
			<option value="19">"19 - Sijoumi, Ezzouhour, El Akba, Dandan"</option>
			<option value="20">"20 - Ben Arous, Madina el jadida"</option>
			<option value="21">"21 - Mourouj, Ibn sina"</option>
			<option value="22">"22 - Sousse, Sfax, Bizerte, Nabeul, Beja"</option>
			<option value="23">"23 - Autre</option>
		</select>
	</div>
</div>
