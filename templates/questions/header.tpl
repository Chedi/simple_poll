<div class="row-fluid">
	<div class="span1">
		<strong>3B</strong>
	</div>
	<div class="span8" align="center">
		<strong>ZARA</strong>
	</div>
	<div class="span2">
		<strong>Decembre 2012</strong>
	</div>
</div>
<fieldset>
	<legend></legend>
	<div class="row-fluid">
		<div class="span4 control-group">
			<label class="control-label span3" for="enqueteur">
				<strong>Enqueteur</strong>
			</label>
			<div class="controls span1">
				<input type="text" name="enqueteur" id="enqueteur" pattern="[a-zA-Z ]*"/>
			</div>
		</div>
		<div class="span4 control-group">
			<label class="control-label span3" for="superviseur">
				<strong>Superviseur</strong>
			</label>
			<div class="controls span1">
				<input type="text" name="superviseur" id="superviseur" value="Ines" pattern="[a-zA-Z ]*"/>
			</div>
		</div>
		<div class="span4 control-group">
			<label class="control-label span3" for="datetimepicker31">
				<strong>Date</strong>
			</label>
			<div class="controls span1">
				<div id="datetimepicker31" class="input-append">
					<input data-format="yyyy-MM-dd" type="text" required="required" name="date_survey"/>
					<span class="add-on">
						<i data-time-icon="icon-time" data-date-icon="icon-calendar"/>
					</span>
				</div>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span4 control-group">
			<label class="control-label span3" for="numero_quest">
				<strong>Quest N°</strong>
			</label>
			<div class="controls span1">
				<input type="text" name="numero_quest" id="numero_quest" required="required" pattern="[0-9]{1,4}"/>
			</div>
		</div>
		<div class="span4"></div>
		<div class="span4 control-group">
			<label class="control-label span3" for="datetimepicker32">
				<strong>Heure</strong>
			</label>
			<div class="controls span1">
				<div id="datetimepicker32" class="input-append">
					<input data-format="hh:mm:ss" type="text" required="required" name="time_survey"/>
					<span class="add-on">
						<i data-time-icon="icon-time" data-date-icon="icon-calendar"/>
					</span>
				</div>
			</div>
		</div>
	</div
</fieldset>
