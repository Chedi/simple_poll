<div class="row-fluid">
       <div class="span1">
               <strong>Q6</strong>
       </div>
       <div class="span10">
               <strong>Si intervieuvé accompagné, pouvez vous nous préciser le lieu de parenté avec la ou les personnes qui vous accompagne ?</strong>
       </div>
</div>
<div class="row-fluid control-group">
       <label class="span3 offset1" for="accompagne_type">
               <input type="checkbox" name="accompagne" id="accompagne"/>Accompagné par
       </label>
       <div class="controls span8">
               <select name="accompagne_type" id="accompagne_type" disabled required="required" multiple="multiple">
                       <option value="0">------</option>
                       <option value="1">Amis</option>
                       <option value="2">Enfant</option>
                       <option value="3">Epoux/Epouse</option>
                       <option value="4">Parents</option>
                       <option value="5">Membre de la famille</option>
					   <option value="6">Seul</option>
               </select>
       </div>
</div>
<div class="row-fluid control-group">
       <label class="span3 offset1" for="nbre_personnes">
               <strong># personnes</strong>
       </label>
       <div class="controls span8">
               <input type="text" name="nbre_personnes" id="nbre_personnes" disabled required="required" pattern="[0-9]{1,3}">
       </div>
</div>
