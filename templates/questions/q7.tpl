<div class="row-fluid">
	<div class="span1 offset1">
		<strong>Q7</strong>
	</div>
	<div class="span10">
		<strong>Signalitique</strong>
	</div>
</div>
<div class="row-fluid">
	<div class="span2 offset2">
		<strong>Sexe</strong>
	</div>
	<div class="span8">
		<select name="sexe" id="sexe">
			<option value="0">--------</option>
			<option value="1">Homme</option>
			<option value="2">Femme</option>
		</select>
	</div>
</div>
<div class="row-fluid">
	<div class="span2 offset2">
		<strong>Age</strong>
	</div>
	<div class="span8">
		<select name="age" id="age">
			<option value="0">--------</option>
			<option value="1">15-24</option>
			<option value="2">25-34</option>
			<option value="3">35-49</option>
			<option value="4">50 et plus</option>
		</select>
	</div>
</div>
<div class="row-fluid">
	<div class="span2 offset2">
		<strong>Statut</strong>
	</div>
	<div class="span8">
		<select name="statut" id="statut">
			<option value="0">--------</option>
			<option value="1">Marié</option>
			<option value="2">Célibataire</option>
			<option value="3">Autre</option>
		</select>
	</div>
</div>
<div class="row-fluid">
	<div class="span2 offset2">
		<strong>Niveau</strong>
	</div>
	<div class="span8">
		<select name="niveau" id="niveau">
			<option value="0">--------</option>
			<option value="1">Primaire</option>
			<option value="2">Secondaire</option>
			<option value="3">Universitaire</option>
		</select>
	</div>
</div>
<div class="row-fluid">
	<div class="span2 offset2">
		<strong>Activité</strong>
	</div>
	<div class="span8">
		<select name="activite" id="Activite">
			<option value="0">--------</option>
			<option value="1">Au Foyer</option>
			<option value="2">Actif</option>
			<option value="3">Etudiant</option>
			<option value="4">Autre</option>
		</select>
	</div>
</div>
<div class="row-fluid">
	<div class="span2 offset2">
		<strong>Profession</strong>
	</div>
	<div class="span8">
		<input type="text" name="profession" id="profession"/>
	</div>
</div>
<div class="row-fluid">
	<div class="span2 offset2">
		<strong>Marque de voiture</strong>
	</div>
	<div class="span8">
		<input type="text" name="voiture" id="voiture"/>
	</div>
</div>
<div class="row-fluid">
	<div class="span2 offset2">
		<strong>Quatier de résidence</strong>
	</div>
	<div class="span8">
		<input type="text" name="residence" id="residence"/>
	</div>
</div>
<div class="row-fluid">
	<div class="span2 offset2">
		<strong>Téléphone</strong>
	</div>
	<div class="span8">
		<input type="text" name="telephone" id="telephone"/>
	</div>
</div>