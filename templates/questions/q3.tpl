<div class="row-fluid control-group">
	<div class="span1">
		<strong>Q3</strong>
	</div>
	<label class="span3" for="comment_arrivez">
		<strong>Comment êtes vous arrivez ?</strong>
	</label>
	<div class="controls span8">
		<select name="comment_arrivez" id="comment_arrivez" required="required">
			<option value="0">--------</option>
			<option value="1">Voiture perso</option>
			<option value="2">Bus ou Métro</option>
			<option value="3">Vélo - Moto</option>
			<option value="4">Taxi</option>
			<option value="5">Accompagné</option>
			<option value="6">A pied</option>
		</select>
	</div>
</div>