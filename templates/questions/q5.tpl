<div class="row-fluid">
	<div class="span1">
		<strong>Q5</strong>
	</div>
	<div class="span10">
		<strong>Pouvez vous me dire combien de fois vous visitez ?</strong>
	</div>
</div>
<div class="row-fluid control-group">
	<label class="span3 offset1" for="visite_magasin_geant">
		<strong>magasin géant</strong>
	</label>
	<div class="controls span8">
		<select name="visite_magasin_geant" id="visite_magasin_geant" disabled>
			<option value="0">--------</option>
			<option value="1">1 fois / semaine</option>
			<option value="2">1 fois / mois</option>
			<option value="3">Plus d'une fois / semaine</option>
			<option value="4">Plus d'une fois / mois</option>
			<option value="5">Que pour un achat particulier - solde - promo</option>
		</select>
	</div>
</div>
<div class="row-fluid control-group">
	<label class="span3 offset1" for="visite_zara">
		<strong>zara</strong>
	</label>
	<div class="controls span8">
		<select name="visite_zara" id="visite_zara">
			<option value="0">--------</option>
			<option value="1">1 fois / semaine</option>
			<option value="2">1 fois / mois</option>
			<option value="3">Plus d'une fois / semaine</option>
			<option value="4">Plus d'une fois / mois</option>
			<option value="5">Que pour un achat particulier - solde - promo</option>
		</select>
	</div>
</div>